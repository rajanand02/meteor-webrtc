Router.configure
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
    
Router.map ->
  @route 'welcome', path: '/'

  @route 'chat', path: 'chat'

  @route 'about', path: 'about'

  
